import React, {useState, useEffect} from 'react';
import Formulario from './components/Formulario'
import ListadoImagenes from './components/ListadoImagenes'

function App() {

  // 16420698-3fadafcfc009778eb75c76025 (api key)
  // ruta https://pixabay.com/api/
  // state de la app
  const [busqueda, setBusqueda] = useState('')
  const [imgenes, setImagenes] = useState([])
  const [paginaactual, setPaginaActual] = useState(1)
  const [totalpagina, setTotalPaginas] = useState(1)

  useEffect(()=>{
    const consultarAPI= async() => {
      if(busqueda==='') return;
    const imageForPage = 30;
    const key= '16420698-3fadafcfc009778eb75c76025'
    const url = `https://pixabay.com/api/?key=${key}&q=${busqueda}&per_page=${imageForPage}&page=${paginaactual}`

    const respuesta = await fetch(url);
    const resultado = await respuesta.json();

    setImagenes(resultado.hits)
    //calcular el total de pagina
    
    const calculatTotalPagina = Math.ceil(resultado.totalHits/imageForPage)
    setTotalPaginas(calculatTotalPagina)

    // mover la pantalla hacia arriba
    const jumbotron = document.querySelector('.jumbotron')
    jumbotron.scrollIntoView({behavior: 'smooth'})
    }
    consultarAPI();
    
  },[busqueda, paginaactual])

  const paginaAnterior = () => {
    const nuevaPaginaActual = paginaactual -1
    if(nuevaPaginaActual===0) return
    setPaginaActual(nuevaPaginaActual)

  }
  const paginaSiguiente = () => {
    const nuevaPaginaActual = paginaactual + 1
    if(nuevaPaginaActual > totalpagina) return
    setPaginaActual(nuevaPaginaActual)
  }
  
  return (
    <div className="container">
      <div className="jumbotron">
        <p className="lead text-center">Buscador de imagenes</p>
        <Formulario
        setBusqueda={setBusqueda}
         />
      </div>
      <div className="row justify-content-center">
        <ListadoImagenes 
        imagenes={imgenes}
        />
        {(paginaactual === 1) ? null : 
        <button 
        type="button"
        className="bbtn btn-info mr-1"
        onClick={paginaAnterior}
        >&laquo; Anterior</button>}

        {(paginaactual===totalpagina) ? null : 
        <button 
        type="button"
        className="bbtn btn-info mr-1"
        onClick={paginaSiguiente}
        >Siguiente &raquo;</button>
        }
      </div>
    </div>
  );
}

export default App;
